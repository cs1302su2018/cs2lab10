
import edu.westga.cs1302.files.controllers.GuiController;

/**
 * This is the driver class for the files application
 * 
 * @author	CS1302
 * @version	Summer 2018
 *
 */
public class TelescopeDriver {

	/**
	 * Entry-point into the application
	 * 
	 * @param args	Not used
	 */
	public static void main(String[] args) {
		GuiController.show();
	}
}
